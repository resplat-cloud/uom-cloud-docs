# Contributing Guide

This contributing guide is primarily for contributions to the
[resplat-cloud/uom-cloud-docs](https://gitlab.unimelb.edu.au/resplat-cloud/uom-cloud-docs) repository.
There are various ways to contribute to our documentation:

* [Bug reports, user experience feedback, and topic requests](#issue-tracker)
* [Merge request](#merge-request-guide)

If you were looking for ways to contribute to the Melbourne Research Cloud dashboard, please refer to its own
[MRC Contribution Guide](https://gitlab.unimelb.edu.au/resplat-cloud/uom-cloud-dashboard/wikis/Contribution-guide).


## Issue Tracker

Bugs, documentation errors, and requests for new topics can be raised using the
[GitLab Issue Tracker](https://gitlab.unimelb.edu.au/resplat-cloud/uom-cloud-docs/issues).
Please provide as much information as possible.
If you encounter a serious issue, such as an unplanned outage, please contact
[rc-support@unimelb.edu.au](mailto:rc-support@unimelb.edu.au).


## Merge Request Guide

You may submit a
[merge request](https://gitlab.unimelb.edu.au/resplat-cloud/uom-cloud-docs/merge_requests)
on GitLab.

### Contribution requirements

Contributions must meet the following requirements:

* Markdown for natural language content should avoid long lines, using significant punctuation such as full stops and semicolons to introduce line breaks.
  See the footnotes for additional resources regarding Markdown style [^1][^2][^3].
* Our documentation should be understandable by people with weak English and/or non-technical backgrounds.
  * Prefer short words, short sentences, and short paragraphs.
  * When jargon is required, either give an accessible definition at the point where it is introduced, or add it to [Concepts](training/concepts).
* Markdown for other repository files (`README.md` and `CONTRIBUTING.md`) may use
  [GitLab Flavoured Markdown (GFM)](https://docs.gitlab.com/ee/user/markdown.html).
* Inline graphics must be optimised for Web (SVG, JPEG, PNG, and GIF):
  * SVG is hard to generate, but when available it is preferred for simple diagrams and logos.
  * JPEG uses lossy compression, making it suitable (and preferred) for natural photographs.
    Do _not_ use JPEG for screenshots/logos.
  * PNG is the modern standard for lossless images and should be preferred for all screenshots.
  * GIF may still be used if importing ancient documentation, but otherwise it is completely superseded by PNG.
    PNG offers full colour and superior compression;
    we do not support ancient browsers which lack support for PNG.
* Python source code must conform to the
  [PEP 8 standard](https://www.python.org/dev/peps/pep-0008/).
* [University of Melbourne policies](https://policy.unimelb.edu.au/category/Facilities%20and%20IT) should be observed where relevant.
* University of Melbourne branding guidelines should be observed.
  See the [Brand Hub](https://brandhub.unimelb.edu.au/) for more details.
* Commits must include a useful message and description.
  It must allow system administrators to understand if any configuration changes are required.
  A rule of thumb is to word commit messages in the imperative voice (i.e. like you are giving a command).
* We primarily support Firefox and Chrome/Chromium.
  These browsers are the primary targets for the Openstack Horizon dashboard, which the MRC dashboard is based on.
  All changes must work in the latest versions of Firefox and Chrome/Chromium.
* If creating a new page in the documentation, remember to add it to `nav` in `./mkdocs.yml`.


### Post-merge Deployment

Accepting a Merge Request does not auto-deploy to the production docs.
Additional steps are required by someone with ssh access to `docs.cloud.unimelb.edu.au`.
(NB: even if you have access, ssh is only available via whitelisted subnets, eg VPN.)

Once logged in, follow these steps:

1. `cd /var/www/html/uom-cloud-docs`
2. `git pull --rebase`
3. `mkdocs build`


## Footnotes

[^1]: ['To Wrap or Not to Wrap: One of Life's Greatest Questions' by Marcia
      Ramos](https://about.gitlab.com/2016/10/11/wrapping-text/)
[^2]: ['Markdown Style Guide - Line wrapping' by Ciro Santilli](http://www.cirosantilli.com/markdown-style-guide/#line-wrapping)
[^3]: [Markdown Links and 80 Character Line Length](http://www.rubycoloredglasses.com/2017/11/markdown-links-80-character-line-length/)
