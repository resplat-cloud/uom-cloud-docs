The Melbourne Research Cloud (MRC) offers free on-demand computing resources to researchers at the University of Melbourne (and affiliated institutions).
It provides similar functionality to commercial cloud providers such as Amazon Web Services (AWS), Microsoft Azure, and Google Cloud Platform.
Common use cases include data analysis and web hosting, but there are many other potential uses.

In short, whenever your own personal computer isn't up to the job, the Melbourne Research Cloud might be the solution.
The MRC is managed by [Research Computing Services].

## Infrastructure

The Melbourne Research Cloud comprises almost 20,000 virtual cores, available across a range of virtual machine sizes.
It also includes specialist resources such as GPGPUs, private networking, load balancing, and DNS.
It is located within the University of Melbourne's Queensberry Hall data centre.


## Accessing the Cloud

The Melbourne Research Cloud can be accessed using your staff or student University of Melbourne account, by logging into the dashboard:

<a class="button-small with-arrow" href="https://dashboard.cloud.unimelb.edu.au/" target="_blank">Melbourne Research Cloud Dashboard</a>

To get started using the MRC, you will need to create a project.
More information about how to do this is at our [Managing Allocation Requests guide](guides/allocations.md).


## Getting Help

A [brief tutorial](training.md) is available to help you get started with the
research cloud, as well as the [FAQs](faq.md) and [guides](guides.md). [Nectar cloud training](guides/nectar_cloud_training.md) is also available externally from ARDC.

If you have technical questions, or would like to discuss your needs with us,
you may request help or further hands on help here:

* UoM Staff Request - [https://go.unimelb.edu.au/tu78](https://go.unimelb.edu.au/tu78)  

* UoM Staff Issue - [https://go.unimelb.edu.au/3u78](https://go.unimelb.edu.au/3u78)  

* UoM Student Request - [https://go.unimelb.edu.au/6o78](https://go.unimelb.edu.au/6o78)  

* UoM Staff Issue - [https://go.unimelb.edu.au/xo78](https://go.unimelb.edu.au/xo78)  

<a class="button-small with-arrow soft" href="https://uomservicehub.service-now.com/esc?id=sc_cat_item&sys_id=5eef1dcbdbe9a74035dc403c3a9619f5" target="_blank">Request Help</a>

## Terms of Service
Please read our [Terms of Service](https://gateway.research.unimelb.edu.au/__data/assets/pdf_file/0009/4672872/RCS-TOS-pub.pdf)

[Research Computing Services]: https://research.unimelb.edu.au/infrastructure/research-computing-services
