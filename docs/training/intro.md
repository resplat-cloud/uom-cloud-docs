# 1. What is the research cloud?

First, a little background on what we mean by _cloud computing_ and its benefits.
You can [safely skip](#do-i-need-cloud-computing)
this section if you're already familiar with similar commercial services like Amazon Web Services (AWS).


## What is the cloud?

The 'cloud' is a overloaded term, so let's clarify what we mean.
Cloud-based services are normally broken down as follows (with a few examples).

<table>
	<thead>
		<tr>
			<td><strong>Infrastructure as a Service (IaaS)</strong></td>
      <td><strong>Platform as a Service (PaaS)</strong></td>
      <td><strong>Software as a Service (SaaS)</strong></td>
		</tr>
	</thead>
	<tr>
		<td>
			<ul>
        <li>Amazon Web Services</li>
        <li>Microsoft Azure</li>
        <li>Melbourne Research Cloud</li>
			</ul>
		</td>
		<td>
      <ul>
        <li>Heroku</li>
        <li>Google App Engine</li>
      </ul>
		</td>
		<td>
      <ul>
        <li>Dropbox</li>
        <li>Google Docs</li>
      </ul>
		</td>
	</tr>
</table>

The Melbourne Research Cloud (MRC) is largely Infrastructure-as-a-Service (IaaS).
That is, it provides raw IT infrastructure like computers, networking and storage.
Higher-level PaaS and SaaS services are built on top of this.
Imagine building a house.
IaaS is the timber, bricks, mortar and nails.
PaaS might be a prefabricated wall, and SaaS is a complete house ready to move into.
Each has trade-offs in terms of flexibility vs. ease-of-use.

Why is IaaS useful?
In the not too distant past, if you wanted a server, you'd have to buy it, install it in a data centre, pay for rent and electricity, and replace it if it failed or became obsolete.
This tied up lots of money and resources.
Companies like Amazon figured out that they could instead rent their spare capacity to customers on a per-hour basis.
Their customers could get access to a computer quickly, have the hardware managed on their behalf, and release it again when they were done, whether it be in a couple of hours, months or years.
Computers and storage could all be tied together in a virtual network, allowing complex applications to be built with very low startup costs.
This paradigm unlocked a multi-billion dollar industry and revolutionised IT.

The Research Cloud has a small Software as a Service aspect, mostly delivering high impact applications — like RStudio and Jupyter — which can leverage the size and durability of our underlying infrastructure.


## Do I need cloud computing?

IaaS can be enormously useful in research. Do any of these apply to you?

- I want a *large number of processors*
- I want a *large amount of memory*
- I want *lots of computers*
- I want to *share my computer* with collaborators
- I want my computer to *host a service* such as a website or database
- I want to use a *different operating system* to my usual physical devices
- I want to launch *long-running tasks* without worrying about power outages or physical access
- I have *bursty* computing needs, i.e. occasional access to a lot more power, temporarily

If any of the above apply, then the Melbourne Research Cloud might be a good fit.
Traditional computers take time to acquire, and have a three or four year asset life which might be beyond the life of your project.
With the Melbourne Research Cloud, you can bring computers online within minutes, and release them again when done, without needing to arrange disposal of physical hardware.


## What about High Performance Computing (HPC)?

Research Computing Services also operates a HPC system called Spartan.
More information about Spartan can be found at
[http://dashboard.hpc.unimelb.edu.au](http://dashboard.hpc.unimelb.edu.au).

Common traits of HPC systems are:

* They run Linux, and interacting with the system is usually done using Secure Shell (aka ssh) to obtain a remote Unix-like command line.
* They have large numbers of processors, permitting massive parallelisation.
* They are a shared environment, meaning all system administration is done for you, jobs need to wait their turn in a queue, and you can't reconfigure the system or use it to expose services to third parties.

If you are performing large-scale data analysis or simulations, either HPC or the Research Cloud may be appropriate.
From our point of view, we have no preference for which service you should use, so long as you are happy.
Some researchers simultaneously use both Spartan and MRC, and we are very excited by this kind of synergy.

You should probably consider HPC first if all the following apply to you:

* Your software runs on Linux.
* Your software is amenable to batch processing (i.e. can be scripted to run non-interactively without a graphical interface).
* You have large resource requirements, in terms of processors and memory.
* You prefer someone else to take care of tasks such as program installation and system administration.
* You are willing to have your compute jobs wait in a queue.

Whereas, you will probably find MRC a better fit if any of the following apply to you:

* You want more control: a variety of operating systems, which can be configured how you like.
* You want to expose services to the external internet.
* You are comfortable doing your own systems administration.
* You don't need a huge amount of resources, and you don't want to wait in a queue to use them.
