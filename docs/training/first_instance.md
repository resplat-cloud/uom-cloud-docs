# Your First Instance

We assume you have already been approved for a Melbourne Research Cloud (MRC) project.
While all users are associated with a trial project, with a name like `pt-XXXXX`, these are not configured to offer usable resources in the MRC.

If you believe the MRC can support your research, you are encouraged to submit a request for a new project.
More information about how to do this is in the [Allocations Guide](../guides/allocations.md).

Once your project is approved, come back to this page to continue learning how to get up and running with your first virtual machine instance.
In the following tutorial, you will bring online an instance running Linux (specifically, Ubuntu 18.04), and connect to it over SSH.

**Let's go!**

## Preparation

### Login to the dashboard

Login to the [Melbourne Research Cloud dashboard](https://dashboard.cloud.unimelb.edu.au/) using your university account.

<a id="create_keypair"></a>
### Create a keypair

Navigate to [Compute -> Key Pairs](https://dashboard.cloud.unimelb.edu.au/project/key_pairs) and click `+ Create Key Pair`.
Give it a name, and you'll then download the private half of the key pair (a file ending in `.pem`).
Keep this file secret, keep it safe.
**Anyone with access to it can log into your instances**.

There is a [guide](/guides/ssh_client/) if you would like more information on SSH.
The public half of the keypair will be kept in the cloud to inject into any instances you launch.

<center>
![Creating keys](../img/CreateKeys.png)
</center>

## Launch an instance

Navigate to ['Instances'](https://dashboard.cloud.unimelb.edu.au/project/instances/) and click 'Launch Instance'.
This will open a dialog, which we will walk through next.

<center>
![Launch instance button](../img/LaunchInstance.png)
</center>

### Instance details

The first tab that will open in the Launch Instance dialog is **Details**:

<center>
![Instance details tab](../img/InstanceName.png)
</center>

You _must_ give an instance name; the other fields can be left as their default values.
(Contrary to popular belief, instance names do not need to be unique.)


### Select source

It isn't possible to create a "blank" instance; all instances are based on a source containing software such as an operating system.
For this tutorial, we will base our instance on Ubuntu Bionic.

Go to the **Source** tab.

* Ensure that the _Select Boot Source_ drop down is set to `Image`.
* You might want to search for `ubuntu` in the _Available_ search box
* Select the image named `NeCTAR Ubuntu 18.04 LTS (Bionic) amd64` by clicking on the 'up' arrow next to it.

<center>
![Instance source tab](../img/instance_source.png)
</center>

Once selected you should see it appear in the section titled _Allocated_.

### Choose flavor

Go to the **Flavor** tab.

* Select the `uom.general.1c4g` flavor by clicking on the 'up' arrow next 
  to it in the 'Available' list.

<center>
![Instance flavor tab](../img/instance_flavor_new.png)
</center>

### Add security groups

7. On the **Security Groups** tab:
    * Enable the `default` and `ssh` security groups.
    * The 'default' security group allows outgoing connections from 
      the instance (e.g. for downloading software), while the 'ssh' group 
      allows incoming connections on the port used for SSH (22) so you can 
      connect to your instance.
    * __N.B.__ If you don't enable the SSH Security Group, you will not be able
      to manage your instance until you do.
    
    ![Instance security group tab](../img/InstanceSecurityGroup.jpg)

### Add public key

8. Under the **Key Pair** tab
    * Make sure your new key pair is selected from the 'Available' list.
    
    ![Instance key pair tab](../img/InstanceKeyPair.jpg)

### Launch!

9. Click on **Launch Instance** at the bottom of the dialog.
    
    ![Instance launch button](../img/InstanceLaunchButton.jpg)

10. Your instance will now be created. It will take a couple minutes to come 
    online, when it does you should see an IP address listed in [your list of 
    instances](https://dashboard.cloud.unimelb.edu.au/project/instances/). 
    If anything does go wrong, delete the resultant instance and do the above 
    steps again.
    
    ![Instance launched page](../img/InstanceLaunchedPage.jpg)

## Connect

11. Open your SSH client, and connect to your instance using the IP address 
    listed in the previous step. The username will be `ubuntu`, and instead of 
    using a password, you should select the key you created in step 2. Setup of 
    your SSH client was described in the prerequisites, you can find the 
    details [here](../guides/ssh_client.md#clients).
   
    __MobaXTerm__: the necessary fields are circled. You will most likely need
    to hit "Advanced SSH settings" to make those settings appear.
    ![mobxterm](../img/mobaxterm_highlights.jpg)
    
    __OS X Terminal__: Here you can see a user move (mv) their mykey.pem into 
    .ssh, change the permissions accordingly, then login to the server. Note 
    that the yes/no challenge will only happen the first time you try to 
    connect.
    ![terminal](../img/osx_terminal.png)

### Try some commands

12. You're now connected to your instance! Try running a command; type the 
    following into the command prompt and hit enter:
    
       ```
       $ echo hello world! > hello.txt
       ```
       
    This will write `hello world!` to a text file called `hello.txt`.
       
    If you type the command `ls` you'll see the file in your current directory, 
    and if you type `cat hello.txt` it will display its contents.
     
    If you struggle with this step, don't forget the 
    [command line tutorial](http://www.ee.surrey.ac.uk/Teaching/Unix/)!

13. Congratulations, you've created your first instance in the Research Cloud! 
14. The cloud is a shared resource. As a result, if you are running an instance, 
    that may very prevent someone else from being able to run their instance. This 
    is one of the reasons why each project has a limit on the set of resources 
    they can use. 

    To be a good neighbour, and to safeguard your project limits, you should 
    only use resources when you need them. When you are finished with them, you 
    should release them so others can use them. So now exit your SSH client by
    pressing ctrl-d or closing the window or by typing 
    
    ```
    $ exit
    ``` 
    
    Then delete your instance by clicking on the checkbox next to it and
    selecting the 'Delete Instance' button.

   ![Instance delete page](../img/InstanceDelete.jpg)

### Failure

You will make mistakes and get frustrated at the Dashboard - we know because we
do too. For me, it's always forgetting to allow access for the appropriate 
ports in Security Groups. Always. Don't panic. If you can, just delete the 
instance and start again. If you can't, try turning it off and on again. Or 
contact us for support.

### Getting Help

If you have technical questions, or would like to discuss your needs with us, 
you may request help or further hands on training here:

<a class="button-small with-arrow soft" href="https://unimelb.service-now.com/research" target="_blank">Request Help</a>

