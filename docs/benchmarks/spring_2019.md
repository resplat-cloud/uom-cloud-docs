# Introducing: MRC Premium & MRC General

The team at the _Melbourne Research Cloud (MRC)_ is excited to announce that we have launched an additional 10,000 virtual cores in our data centre to replace our end-of-life Supermicro hypervisors in Q1 2019.
This hardware replacement is made possible by the _University of Melbourne Petascale Campus Initiative_.

Our new hypervisors are divided into two partitions:

1. **MRC Premium**: Faster CPU clock speed, higher physical RAM per hypervisor for more intensive workloads.
2. **MRC General**: Available for all MRC users.

Our performance benchmark shows the new hypervisors significantly out-perform our end-of-life Supermicro fleet in the NeCTAR cloud.

# CPU performance

  ![](../img/0319_mrcbench_coremark.png)

As shown in the benchmarking graph, the CPU performance of the new hypervisors shows a significant boost of **2.3 times** for the _General_ partition and **2.7 times** for the _Premium_ partition, over the Supermicro fleet.
The _General_ partition also shows comparable performance to our current high performance cloud fleet in `melbourne_qh2` installed in 2015.

| Type                 | Score     | Runtime (seconds) |
| :---                 | :---:     |              ---: |
| **MRC Premium**      | **52452** | **301**           |
| NeCTAR melbourne-qh2 | 47181     | 314               |
| **MRC General**      | **44665** | **303**           |
| NeCTAR Supermicro    | 19051     | 367               |

<small>All _CPU Benchmark_ is run on _m3.small_ VM running Ubuntu 18.04 LTS with 2 vCPUs and 4096 MBs of RAM using [CoreMark](https://www.eembc.org/coremark/)</small>

# Network performance

  ![](../img/0319_mrcbench_iperf.png)

Network speed also has a significant improvement over our current hardware.
The new hypervisors' throughput is **1.6 times** more than for the old SuperMicro fleet.

| Type                 | Average Throughput (Mb/s) |
| :---                 | :---:                     |
| **MRC Premium**      | **11444**                 |
| **MRC General**      | **11631**                 |
| NeCTAR melbourne-qh2 | 7558                      |
| NeCTAR Supermicro    | 6820                      |

<small>All tests were performed on 2x _uom.general.4c16g_ VM with 4 vCPUs and 16384 MBs of RAM running on Ubuntu 18.04 LTS using [iPerf](https://iperf.fr/)</small>

# Disk performance

  ![](../img/0319_mrcbench_rootdisk.png)

Root disk performance sees an increase of **at least 2.1 times** compared to VMs on the old Supermicro hypervisors.
Write speed also see an improvement of **3 times** over the Supermicro fleet.

| Type                 | Sequential Write (KB/s) | Sequential Read (KB/s) |
| :---                 | :---:                   | :---:                  |
| **MRC Premium**      | **964413**              | **2010536**            |
| **MRC General**      | **970990**              | **2296492**            |
| NeCTAR melbourne-qh2 | 814460                  | 874487                 |
| NeCTAR Supermicro    | 326225                  | 324737                 |

<small>All tests were performed on a _uom.general.4c16g_ VM  with 4 vCPUs and 16384 MBs of RAM running on Ubuntu 18.04 LTS using [Fio](https://github.com/axboe/fio)</small>

# Attached volume performance

  ![](../img/0319_mrcbench_cinder.png)

On the new hypervisors, VMs with attached volumes can see increases in disk read performance of **at least 2.1 times**.
Write speed, however, stays consistent between all test VMs.

| Type                 | Sequential Write (KB/s) | Sequential Read (KB/s) |
| :---                 | :---:                   | :---:                  |
| **MRC Premium**      | **773436**              | **2224727**            |
| **MRC General**      | **812770**              | **1877947**            |
| NeCTAR melbourne-qh2 | 626984                  | 865308                 |
| NeCTAR Supermicro    | 879892                  | 641625                 |

<small>All tests were performed on a _uom.general.4c16g_ VM  with 4 vCPUs and 16384 MBs of RAM running on Ubuntu 18.04 LTS using [Fio](https://github.com/axboe/fio)</small>

