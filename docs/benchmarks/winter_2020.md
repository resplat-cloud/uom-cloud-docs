# MRC General 2020 Hardware Refresh

The team at the _Melbourne Research Cloud (MRC)_ is excited to announce that we have launched 11,264 virtual cores in our data centre to replace 9,600 virtual cores in **MRC General** hypervisors that has reached end-of-life.
This hardware replacement is made possible by the _University of Melbourne Petascale Campus Initiative_.

Our fleet of replacement hypervisors are built on AMD EPYC 2 with a base CPU clock speed of 2.0Ghz and can be bursted to 3.35Ghz, offering significant performance to the outgoing Intel Xeon 2.3Ghz.

# CPU performance

  ![](../img/0620_mrcbench_coremark.png)

The AMD hypervisors offer **21%** improvement over the EOL hypervisors using Ubuntu 18.04. On Ubuntu 20.04, which on kernel 5.x and able to take full advantage of the AMD Zen architecture, the performance gain is as much as **37%**.
The new hypervisors also offer up to **7%** performance gain to the premium _MRC Hicores_ partition.

| Type                        | Score     |
| :---                        | :---:     |
| **MRC AMD on Ubuntu 18.04** | **92935** |
| **MRC AMD on Ubuntu 20.04** | **99612** |
| MRC EOL on Ubuntu 18.04     | 76476     |
| MRC EOL on Ubuntu 20.04     | 72685     |
| MRC Hicores on Ubuntu 18.04 | 92814     |
| MRC Hicores on Ubuntu 20.04 | 93221     |

<small>All _CPU Benchmark_ is run on _uom.general.4c16g_ VM running Ubuntu with 4 vCPUs and 16 GBs RAM using [CoreMark](https://www.eembc.org/coremark/)</small>

# Disk performance

  ![](../img/0620_mrcbench_rootdisk.png)

In term of root disk performance, the AMD hypervisors offer around **15%** improvement over the _MRC General EOL_ and _MRC Hicores_.

| Type             | Sequential Write (KB/s) | Sequential Read (KB/s) |
| :---             | :---:                   | :---:                  |
| **MRC AMD**      | **774583**              | **2509948**            |
| MRC EOL          | 675380                  | 2296592                |
| MRC Hicores      | 674798                  | 2010536                |

<small>All tests were performed on a _uom.general.4c16g_ VM  with 4 vCPUs and 16 GBs of RAM running on Ubuntu 18.04 LTS using [Fio](https://github.com/axboe/fio). Due of the nature of networked storage, performance metric might fluctuate based on network traffics.</small>

# Attached volume performance

  ![](../img/0620_mrcbench_cinder.png)

On the new hypervisors, VMs with attached volumes can see an average increases disk write performance from **20%-25%** and read performance from **30%-45%**.

| Type           | Sequential Write (KB/s) | Sequential Read (KB/s) |
| :---           | :---:                   | :---:                  |
| **MRC AMD**    | **827368**              | **2971931**            |
| MRC EOL        | 762516                  | 1877947                |
| MRC Hicores    | 574544                  | 2224727                |

<small>All tests were performed on a _uom.general.4c16g_ VM  with 4 vCPUs and 16 GBs of RAM running on Ubuntu 18.04 LTS using [Fio](https://github.com/axboe/fio). Due of the nature of networked storage, performance metric might fluctuate based on network traffics.</small>
