# Using the UoM Internal Network

When launching an instance, there are at least three networks you can choose from:

* `Classic Provider`
* `qh2-uom`
* `qh2-uom-internal`

The _Classic Provider_ network is chosen by default;
it is equivalent to manually choosing the `qh2-uom-internal` network, which provides your instance with a private (i.e. not world accessible) IP address.
Unlike `qh2-uom`, the `qh2-uom-internal` network does _not_ expose your instance to the public internet.

(You may see other networks available if your project has [Advanced Networking](advanced_networking.md) quota.)

<center>
![launch instance: networks](../img/launch_instance_networks.png)
</center>

## Downsides to the Public Internet

### Security

The public internet is home to powerful botnets constantly looking for vulnerable devices.
If you monitor the traffic on any public-facing network device, you can see hundreds or thousands of attack attempts per day.

Our official Linux images have secure default configurations, but no amount of security guarantees immunity.
Furthermore, once you start customising the services and configuration on your instance, or if you use a non-Linux image, security becomes much less sure.

### Scarcity

There are fewer than four billion IP addresses available on the public internet.
This is about one for every two people on the planet.
As such, obtaining a pool of public IP addresses is expensive, and not getting any cheaper.

The `qh2-uom` network has a little over 2500 IP addresses which need to be shared between roughly the same number of users.
Many users like to have more than one virtual machine.
The `qh2-uom-internal` network, on the other hand, has twice as many IP addresses, and we have more flexibility to add more subnets if we run out.

This has practical effects.
Our pool of public IP addresses does occasionally become completely exhausted.
This leads to errors when trying to launch new instances, which is naturally frustrating for the affected users.

Thus, even for instances with comfortable security, choosing an internal IP address can be a kindness to people who do need public-facing virtual machines.


## Launching a new instance using UoM Internal Network on Melbourne Research Cloud

**WARNING:** When launching your instance in this network, it will only be accessible when you are on campus.
If you would like to access your instance from outside the University, you will need to use the [University of Melbourne Virtual Private Network (VPN)](https://studentit.unimelb.edu.au/findconnect/vpn).

Launching a new instance using UoM Internal Network follows the same process as documented in
[https://docs.cloud.unimelb.edu.au/training/first_instance/](https://docs.cloud.unimelb.edu.au/training/first_instance/).
In the Network section, select _Classic Provider_ or `qh2-uom-internal`:

<center>
![overview](../img/qh2-uom-internal.png)
</center>


