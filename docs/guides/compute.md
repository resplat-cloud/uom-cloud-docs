# Running basic applications on the cloud

A common use case is to simply use your instance on the cloud to run 
applications.

To do this you will need to know:

* The applications licensing terms (some companies have very different licences 
  for running on the cloud);
* How to install and configure the required applications on your instances; 
* How to move data to and from your instances;
* How to make data available to the applications on the instances.

## Volumes

Volume storage is analogous to a USB drive: you attach it to your running instance, then detach it before you delete the instance.
In this way, your data can live on beyond the life of your instances.

One limitation to be aware of is that a Volume can only be connected to 
a single running instance at a time.

If you decide you need Volume storage, remember to include it in your project 
[allocation request](allocations.md). 

## Stopping your instances

When you are finished using your applications you should remember to destroy
your instances.

There are several different ways of stopping your running instance:

* A paused instance is simply halted in memory, ready to be started again. 
* A stopped instance has its state written to the host disk, ready to 
  be resumed from there. 
* A soft reboot tries to do a graceful reboot (you ask the OS to reboot). 
* A hard reboot forcibly reboots the instance! (you press the reset button)
* A shut off instance is one that has been gracefully powered down.
* A deleted instance is one that has been forcibly terminated and all traces
  removed from the host.

**NB** Know that if you don't delete your instance then it will still be 
counted against your allocation. Stopped and paused instances are still 
consuming resources on their host!

## Snapshots

There are two types of snapshots you can make:

* An _instance snapshot_.
* A _volume snapshot_.

There are certain similarities between them, although there are also important
differences. They both create a copy of the current state of an instance or
snapshot respectively. Your instance snapshots appear in the dashboard under the
Compute -> Images tab. Volume snapshots are under Volumes -> Snapshots.

You can use an instance snapshot as a template when launching a new instance.
You can use a volume snapshot to create a new volume. It will be identical to
how the original volume was at the time of the snapshot.

It is possible to convert an instance snapshot into a volume via the entries in
Compute -> Images. This could be useful if you want a boot-from-volume instance
based on one of your instance snapshots.

**N.B.** If you make a snapshot of a running instance, you will **only** copy 
the primary disk, **not** any attached volume storage.

## Moving data to and from your instance

There are many applications you can use to move data to and from
your instance. Some common ones are:

* rsync - a unix command line utility for transferring and synchronising files 
  across computer systems;
* sftp - a command line file transfer program that uses the SSH connection to 
  transfer the files;
* scp - secure copy, another command line utility that uses the SSH connection
  to transfer the files;
* [Cyberduck](https://cyberduck.io/), 
  [FileZilla](https://filezilla-project.org/) - programs with a graphical user 
  interface. They run on both Windows and macOS. [WinSCP](https://winscp.net/)
  is an easy to use Windows only program.
* database backup/restore - if your application has a relational database, the
  chances are good that the database supports backing up and restoring of its
  contents.
* application import/export utilities - a lot of applications support the
  export/import of data directly, either via their user interface or 
  programmatically. Be wary of data uploads through the web - they will often
  be very slow compared to other methods listed. 
  
The best path here is entirely dependent on your applications and the way in
which you are using them. We have basic walkthroughs in the 
[file transfers](file_transfer.md) guide.

## Help

If you have technical questions, or would like to discuss your needs with us, 
you may request help or further hands on training here:

<a class="button-small with-arrow soft" href="https://unimelb.service-now.com/research" target="_blank">Request Help</a>

