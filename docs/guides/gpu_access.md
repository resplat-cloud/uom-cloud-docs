# Accessing the special GPU resources

GPUs are not available through the Melbourne Research Cloud any more. Due to oversubscription, we have made them available through the [Research Computing Portal](https://gateway.research.unimelb.edu.au/platforms-data-and-reporting/data-and-computation/research-computing-services-rcs/news2/introducing-rcp-research-computing-portal)  

Once you create a Research Server in the RCP, you can request (via a support ticket) to have your Research Server resized to a GPU flavour, for a period of 28 days. See [Research Server guides](https://rcp.research.unimelb.edu.au/research_server/information_and_guides/)  
