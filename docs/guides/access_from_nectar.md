# Getting Access for an existing Nectar Project

## What is the difference between Melbourne Research Cloud and Nectar?

There are a lot of similarities between the Melbourne Research Cloud (MRC) and the Nectar Research Cloud (simply called Nectar).
The MRC grew out of Nectar, and they still share some infrastructure.
For example, MRC relies on Nectar for authentication.

They are also both primarily focused on Infrastructure As A Service (IaaS).
IaaS generally refers to offering virtualised compute, networking, and storage.

So, if you were already familiar with how to manage a project and its resources on Nectar, you will feel immediately at home in MRC.

The difference between them is that the MRC is optimised for the needs of University of Melbourne researchers:

* All MRC projects are given access to Windows images.
* All MRC projects are given access to custom "flavors" which grant more memory per core when launching a virtual machine.
* The MRC dashboard removes features which are known to be buggy.
* The MRC dashboard support system is integrated with the University of Melbourne's ServiceNow help desk.
* The MRC runs on hardware specially chosen and purchased by the University of Melbourne to offer an immense amount of computational power.
* The MRC runs completely out of our very own Queensberry Hall Data Centre.
* The MRC is engineered and supported by your fellow University of Melbourne staff, some of whom have previously been University of Melbourne researchers.


## Who can use MRC? Am I able to stay on Nectar if I want to?

The decision is made on a project-by-project basis.
It depends on factors such as how is the project funded, who is it supporting, and who is the Chief Investigator.

It is possible for someone to need to use Nectar for some projects and MRC for other projects.

If your project is funded by a national grant such as ARC or NHMRC, then you should request a project on Nectar first.
This is regardless of project leadership or membership.
A project consisting entirely of University of Melbourne researchers may still be required to operate as a National Merit project on Nectar, if the project has national funding sources.

Whereas, if you don't have any real grants, or all your funding sources are local, and if everyone in the project is based at University of Melbourne, then you should definitely request an MRC project if you think we can support your research.
To do this, please see our guide on [Managing Allocation Requests](../allocations).

If you have a case that seems ambiguous, feel free to raise a ticket as described below.
We will help you to figure out what should be done.


## Getting access for an existing Nectar Project

For the most part, we have already identified and imported all the projects from Nectar which we think are eligible and would benefit from MRC resources.

If you think your project has been overlooked, or if you are no longer eligible to use Nectar (e.g. because your project lost National Merit status), we encourage you to request conversion.
Please click here:

<a class="button-small with-arrow soft" href="https://unimelb.service-now.com/research" target="_blank">Request Help</a>

Make sure to let us know the name of the project you want us to review for importing.
It may also help to provide reasons or information in support of your request.

While waiting for the request to be reviewed, you may continue to use the [Nectar dashboard](https://dashboard.rc.nectar.org.au/).


## Transferring resources from other Nectar Nodes to the Melbourne Research Cloud

It is possible for a project to be imported from Nectar to the Melbourne Research Cloud while that project is still using resources at other nodes.
However, once your project is imported in this way, you will be required to migrate those resources at other Nodes to MRC.

We have a [guide](migrating_to_mrc.md) which walks you through this task.
