# New compute nodes 2024

The MRC team is excited to announce that we have just launched 15360 new virtual cores in the RCS datacentre at the University of Melbourne.
 
These cores are spread across forty new hardware hosts/hypervisors. The CPUs are faster, and the network capacity on the new nodes is double that of those previously available.
 
The new cores are on the AMD EPYC 9474F 48-Core Processor, which has a CPU clock speed of 3600 MHz with a turbo boost of 4100 MHz.

The new nodes also have the Nvidia Mellanox CONNECTX-6 D network card installed `Mellanox® ConnectX-6 DX Dual Port 100GbE QSFP56 Network Adapter`, connected at 2x50Gbps. This is double the bandwidth capacity compared to the older compute nodes.

These hosts will be able to launch a new generation of the widely-used uom.general flavors, uom.general v3:
 
 
 | older flavor        | new AMD Genoa flavor   |
 |---------------------|------------------------|
 | uom.general.32c128g | uom.general.32c128g.v3 |
 | uom.general.8c32g   | uom.general.8c32g.v3   |
 | uom.general.24c96g  | uom.general.24c96g.v3  |
 | uom.general.20c80g  | uom.general.20c80g.v3  |
 | uom.general.12c48g  | uom.general.12c48g.v3  |
 | uom.general.48c192g | uom.general.48c192g.v3 |
 | uom.general.2c8g    | uom.general.2c8g.v3    |
 | uom.general.16c64g  | uom.general.16c64g.v3  |
 | uom.general.28c112g | uom.general.28c112g.v3 |
 | uom.general.4c16g   | uom.general.4c16g.v3   |
 | uom.general.1c4g    | uom.general.1c4g.v3    |
 | uom.general.64c256g | uom.general.64c256g.v3 |
 
There is a 1:1 correspondence between the flavor types, so you will be able to resize your VM to the new 'v3' flavor to gain a performance boost without consuming any additional vCPU allocation.

## Resize process: MRC dashboard

To resize your instance please follow the instructions below:

click on the 'Actions' drop-down at the far right of the column with the affected virtual machine.

![](../img/genoa_resize.png)

Once the menu appears, you will be able to select a new flavor.

![](../img/genoa_resize_flavor.png)

To launch on the new hardware, select the same flavor name with the `v3` suffix.

During the resize process, your virtual machine will reboot and be inaccessible until the resize is complete.

## Resize process: Openstack CLI

To resize using the Openstack CLI, use the following command structure:

```
openstack server resize --flavor <target-flavor> <server-ID>
```
