# Creating Volumes

Our instances only come with 30GB drives - enough for the OS and a little over.

In reality, we will need more for our data. Volumes are the solution. Basically
an external drive, here we will create, format and mount one on an instance.

Volumes are an excellent solution when it comes to getting your data closer to 
your compute. They can be backed up through this interface as well. 

For longer term storage solutions - once it's time for the data to be archived,
there are better solutions. But Volumes are great for the short term work.

This guide presumes you have an instance created.

1. To create a Volume, go to the [Volumes > Volumes](/project/volumes/) on the 
   dashboard and click `Create Volume`.

	![](../img/create_volume_1a.png)

2. Fill in the details, making the Volume large enough to fit your data.

	![](../img/create_volume_2.png)

3. To attach it to an instance, select "Manage Attachments" from the drop down
   There is a single caveat - both the instance and the Volume will need to be
   in the same Availablility Zone.

	![](../img/create_volume_manage_attach_a.png)

4. Once successful, you will see where it is attached to the instance in 
   question. In this image, you will see that it's been added to `/dev/vdb`

	![](../img/create_volume_attached.png)

## Formatting the Volume
At this point, the Volume is only attached to the instance in question. If the 
Volume was created with `Volume Source` set to "No source, empty volume" we 
still need to format the drive.

To format the volume, take note of where it is attached, and run
```
ubuntu@dev-doc-1:~$ sudo mkfs -t ext4 /dev/vdb
mke2fs 1.44.1 (24-Mar-2018)
Creating filesystem with 13107200 4k blocks and 3276800 inodes
Filesystem UUID: 24db0256-4a5f-458d-8f95-36b28fa6a352
Superblock backups stored on blocks: 
   32768, 98304, 163840, 229376, 294912, 819200, 884736, 1605632, 2654208, 
   4096000, 7962624, 11239424

Allocating group tables: done                            
Writing inode tables: done                            
Creating journal (65536 blocks): done
Writing superblocks and filesystem accounting information: done   

ubuntu@dev-doc-1:~$ 
```
    
## Mounting the Volume

To be used, the last step is to mount the drive. In this case I will make a 
directory at the root level called `data` and mount the volume there

```
ubuntu@dev-doc-1:~$ sudo mkdir /data
ubuntu@dev-doc-1:~$ sudo mount /dev/vdb /data
```

You will now be able to copy or write your data to the 50GB Volume mounted at 
`/data`

```
ubuntu@dev-doc-1:~$ df -H
Filesystem      Size  Used Avail Use% Mounted on
udev            2.1G     0  2.1G   0% /dev
tmpfs           414M  3.0M  411M   1% /run
/dev/vda1        32G  2.0G   29G   7% /
tmpfs           2.1G     0  2.1G   0% /dev/shm
tmpfs           5.3M     0  5.3M   0% /run/lock
tmpfs           2.1G     0  2.1G   0% /sys/fs/cgroup
tmpfs           414M     0  414M   0% /run/user/1000
/dev/vdb         53G   55M   50G   1% /data
```

## Permanently mounting the Volume
Unfortunately, that mount will not persist over a reboot. The data will be safe,
but the mount will disappear and you will need to run the `sudo mount` command
again.

To prevent this, we can make the mount permanent. To do this, we will add an 
entry to the configuration file that controls the mounted volumes, 
`/etc/fstab`.  Using the text editor of your choice, add the following line to
the bottom of the file
`/dev/vdb        /data   auto    defaults      0       0`

You can now rest easy knowing that your Volume will always be mounted at /data
when you reboot your instance.

