# Server

Running a 24/7 web or analysis application on a cloud instance is a popular 
choice. 

## Security

Because the instance is always up, and because the address is well
known, you can expect web servers to receive a lot of interest from people who
are not well-intentioned. Hence you should pay extra attention to the security
of your server.

Security is a very broad and deep topic. 

Here are some of our key recommendations:

* Prefer SSH access over password access
* Keep your SSH private keys secure
* Make sure that you use appropriate security groups 
  (only open on needed ports)
* Have the firewall on your instance enabled
* Try to have automatic security upgrades set for your instances
* Find and subscribe to your software providers security mailing lists
* Encrypt any filesystem that has sensitive data on it
* Consider using networks (if you have configurable network access) to zone
  your instances off of the internet
* Use SSL ([Let's Encrypt](https://letsencrypt.org/) is an excellent, free, 
  option), and [scan](https://www.ssllabs.com/ssltest/) your server to 
  check for vulnerabilities. 
* Follow and act on the security alerts for your chosen operating system and
  application stack

The cloud team conduct routine and random audits to determine the integrity of 
machine images and running instances. If you have discovered a critical 
security flaw, or believe your machine has been compromised, please 
<a class="button-small with-arrow soft" href="https://unimelb.service-now.com/research" target="_blank">Request Help</a>

For for non-urgent security questions lodge a ticket:

<a class="button-small with-arrow soft" href="https://unimelb.service-now.com/research" target="_blank">Request Security Help</a>

If you decide you need configurable networking, remember to include it in your 
project [allocation request](allocations.md). 

## Availability

If it is important that your server is up and running at all times:

* Have logging configured (and look at your logs!);
* Use monitoring and notification software;
* Monitor disk usage: applications don't work well if the disk is full;
* Use [architectural patterns](http://cloudpatterns.org/mechanisms/failover_system) 
  that handle server failure. Consider using networking to provide load 
  balancers;
* If your web server is becoming unresponsive, you need to understand where the
  bottleneck is happening before you fix the problem. It will usually become
  bounded by one or more of: Memory, CPU, Disk I/O, Network I/O
* In your monitoring track your resource usage. Then you should be
  able to address the bottlenecks as they occur
* Consider serving your static assets from the object store or into a
  Content Delivery Network

If you decide you need configurable networking, remember to include it in your 
project [allocation request](allocations.md). 

## Help

Setting up a scalable 24/7 web site on the cloud is one of the more complex 
cloud tasks. So, if you have technical questions, or would like to discuss 
your needs with us, please request help or further hands on training here:

<a class="button-small with-arrow soft" href="https://unimelb.service-now.com/research" target="_blank">Request Help</a>

