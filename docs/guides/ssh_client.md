# Setting up an SSH Client

For cloud based Linux servers, you will have to connect using a protocol called
[Secure SHell (SSH)] to install software, to configure your environment - to 
manage your server. To connect via SSH you will need a keypair.

## Keypair
A _keypair_ is a pair of files used to allow access and encrypt communications.
Each pair consists of a *public* and a *private* file. The public file is 
shared freely with the world. The private file should be kept securely locked 
away, much like a password.
 
When you create a new server instance, your public key will be injected as part
of the build process. You can then use the private key to gain access to the 
instance - and guarantee that your communication is encrypted. This is 
more secure than using passwords. Passwords have the following problems:

* **So** much typing. _So_ very much typing.
* One more thing to remember (and forget)
* Need to be ever more complex
* Easy to crack (especially by social engineering)
* Prone to being shared
* Cascade to other devices if you use the same password across devices. And 
  if you're spawning multiple computers on the cloud are you going to give them 
  all the same password, or somehow 'remember' different ones?

If you're interested in learning more about how keypairs work, recall that 
[public key cryptography for non geeks] provides a useful analogy, and, 
[how encryption works explained] gives a good worked example.

The private file often has the extension [`.pem`]  
(**P**rivacy-**E**nhanced **M**ail).

To generate a keypair we have created a [checklist] (PDF, 53KB) you can follow.
 
## Securing your private key

If you do not secure your private key your first attempt to connect to your 
instance should be met with a message along the following lines:

<pre><code>@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@         WARNING: UNPROTECTED PRIVATE KEY FILE!          @
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
Permissions 0777 for 'mykey.pem' are too open.
It is required that your private key files are NOT accessible by others.
This private key will be ignored.
bad permissions: ignore key: mykey.pem
ubuntu@144.6.225.224's password:</code></pre>

This is happening as the SSH application recognises that anyone on your 
computer can access this file. To rectify it, you need to run the following 
command:

```
$ chmod u=rw,go-rwx ~/mykey.pem
```

Loosely translated, the above means: CHange file MODe for mykey.pem: only I 
can read and write it, no-one else can.

Once you have secured your file, you should be able to connect successfully 
with SSH.

Also, consider putting all of your keypairs into a single directory so you 
know where they are! Traditionally this has been in a directory called "dot
ssh" inside your home directory (e.g. /home/username/**.ssh/**). If you have 
a private key that is very important, consider printing a copy out and putting 
the printout in a fireproof safe! Then if the worst happens, you can type it 
back in again...

##Clients
### Windows
Windows doesn't have an SSH client installed by default.

We suggest [MobaXTerm]

Download either:

* Portable edition, and just unzip it
* Installer edition (may require administrator privileges on your machine)

Create a new session. For example, to connect to a computer with IP 
address `123.123.123.123`, username `ubuntu`, with private key `mykey.pem`:
 
 ![mobxterm](../img/mobaxterm_highlights.jpg)


### Mac (OS X)
OS X already has an SSH client built-in, so there is nothing to install.

To use the SSH client, open the terminal by going to 
Applications -> Utilities -> Terminal

For example, if you saved your key to your home directory as `mykey.pem`, to 
connect to the computer with IP address `123.123.123.123` and username 
`ubuntu`, we type:

```ssh -i ~/mykey.pem ubuntu@123.123.123.123```

If you saved it to .ssh

```ssh -i ~/.ssh/mykey.pem ubuntu@123.123.123.123```


[Secure SHell (SSH)]: https://en.wikipedia.org/wiki/Secure_Shell
[public key cryptography for non geeks]: https://blog.vrypan.net/2013/08/28/public-key-cryptography-for-non-geeks/
[how encryption works explained]: https://www.abc.net.au/news/2017-09-15/how-encryption-works-explained/8732148
[`.pem`]: https://en.wikipedia.org/wiki/X.509#Certificate_filename_extensions
[checklist]: ../checklists/ChecklistCreatingAKeypair.pdf
[MobaXTerm]: http://mobaxterm.mobatek.net/download-home-edition.html
