# Launching a Windows Instance

## 1. Preparation

In preparation for launching a Windows instance, you will need to create a
Volume which will be used as the target for the Windows OS installation.

Firstly, ensure you have sufficient volume quota available in your project.
Confirm current quota by selecting `Compute -> Overview`

![](../img/windows_1.png)

Suggested volume size of 100GB per instance, plus any additional space needed
for your software.
If your allocation does not include sufficient volume storage, you can request
resources in the [Allocations](allocations.md) menu in the sidebar.

1. To create a Volume, go to [Volumes > Volumes](/project/volumes/) on the
   dashboard and click `Create Volume`.

	![](../img/create_volume_1a.png)

2. Fill in the details, making the Volume large enough to install Windows and
 your applications (e.g. 100GB), ensure `Volume Source` shows `No source,
 empty volume` and then select `Create Volume`

	![](../img/windows_6.png)

3. Select `Edit Volume` for your newly created volume and ensure the `Bootable`
 checkbox is ticked and then `Submit`

	![](../img/windows_7.png)


## 2. Create a Windows instance

Head to the `Compute -> Instances` item in menu on the left and navigate to
`Launch Instance`

![](../img/windows_3.png)

Provide a name for the instance, then select `Source`.  
Under `Select Boot Source` select `Image`.  
Then search for `UoM-Cloud` and select the required Windows OS install ISO.

- `Windows 10`:  
`UoM-Cloud-Windows_10_EDU_21H2_64Bit_virtio_0_1_229.iso`

- `Windows Server 2016`:  
`UoM-Cloud-Windows_Svr_Std_and_DataCtr_2016_64Bit_virtio_0_1_126.iso`

![](../img/windows_source2_xlarge.png)

Complete the rest of the `Launch Instance` options including `Flavor`,
`Security Groups`, `Key Pair`, `Networks` etc.

**Important: If you do not require a public IP address for your instance it is
strongly encouraged to select `Classic Provider` or 
`qh2-uom-internal` under `Networks`.** 

When ready, select `Launch Instance`.

Your Windows machine will now start, booting from the install image (this is
like booting from an install DVD or USB drive).

When the instance's status is `Active`, in the `Actions` column select the
dropdown-menu and choose `Attach Volume`

![](../img/windows_8.png)

Select the volume you created in the `Preparation` steps earlier and `Attach
 Volume`

After the volume is attached, reboot your instance by selecting the `Actions`
dropdown-menu again and choose `Hard Reboot Instance`

When the instance shows status `Active`, select `Console` from the `Actions`
dropdown-menu.

Work through the install steps.
![](../img/windows_4.png)

If installing Windows Server, `Windows Server 2016 Standard (Desktop
  Experience)` is suitable for most purposes.
  ![](../img/windows_24.png)

Select the `Custom: Install Windows only (advanced)` install option.
![](../img/windows_25.png)


Your attached Volume should appear as an option in the `Where do you want to
install Windows?` screen. If not, reboot your instance.

![](../img/windows_9.png)

Complete the install steps and set a strong password for your user account.

### Important notes
- Make sure you have a strong password, and that guest access is disabled.
- Make sure automatic updates are enabled, and periodically check that
security updates are being applied.
- If you have sensitive data, consider encrypting it (e.g. using the
  BitLocker tool built into Windows).
- If possible, keep your Windows instance shut down when you're not using it,
 or at least remove the RDP security group so that remote access is disabled.

## 3. Activating Windows

Windows activation requires the date/time to be set correctly on your instance.  

1. **Right-click** on the clock near the bottom-right corner of the taskbar and
 select `Adjust date/time`  
![](../img/windows_15.png)

2. Scroll down and select `Additional date, time, & regional settings`  
![](../img/windows_16.png)

3. Select `Set the time and date`  
![](../img/windows_17.png)

4. Select the `Internet Time` tab and `Change settings...`  
![](../img/windows_18.png)

5. Change the `Server` to `ntp.unimelb.edu.au` and select `Update now` then
`OK`  
![](../img/windows_19.png)

6. Copy and Paste doesn't work through the in-browser Console. The following 
commands will be easier if you can copy/paste. To do so, you will need to 
launch a browser in the console and visit this site 
`docs.cloud.unimelb.edu.au/guides/windows`<br /><br />
**Right-click** the Windows Start icon in the bottom-left corner and select
`Command Prompt (Admin)`  
Select `Yes` to continue  
![](../img/windows_20.png)

7. Type in the relevant command for the version of Windows you are installing 
and then press Enter.<br /><br />
Windows 10 Enterprise:
<pre>
slmgr /ipk NPPR9-FWDCX-D2C8J-H872K-2YT43
</pre>
Windows Server 2016 Standard:
<pre>
slmgr /ipk WC2BQ-8NRM3-FDDYY-2BFGV-KHKQY
</pre>
Windows Server 2022 Standard:
<pre>
slmgr /ipk VDYBN-27WPP-V4HQT-9VMD4-VMK7H
</pre>
![](../img/windows_21.png)
After a few seconds you should receive a pop-up confirmation  
![](../img/windows_22.png)

8. Then type in the following command and press Enter:  
<pre>
slmgr /skms-domain unimelb.edu.au
</pre>
![](../img/windows_26.png)

9. Then reboot your instance. After logging back in your Windows instance should be automatically activated. To confirm, open `System` by right-clicking the Windows start icon in the bottom-left corner and select `System`  
![](../img/windows_11.png)

10. Confirm that `Windows is activated`  
![](../img/windows_27.png)

11. If activation did not complete automatically, then open a Command Prompt
 again by right-clicking the Windows Start icon in the bottom-left corner and
 select `Command Prompt (Admin)`. Select `Yes` to continue  
![](../img/windows_20.png)

12. Type in the following command and press Enter:  
<pre>
slmgr /ato
</pre>
After 10-15 seconds you should receive a pop-up confirmation stating the
activation was successful  
![](../img/windows_23.png)


## 4. How to enable Remote Desktop connection on your Windows instance

While the Console interface in the Dashboard is functional, it's probably not
ideal for most users (for example, you can’t share files, copy and paste, or
get a full screen interface). You can enable Remote Desktop connections to
your instance with the following steps:

Follow the steps below to create a security group named 'RDP', configure an
'RDP' rule, and apply the security group to your instance. This will make sure
the necessary ports are open on your instance to receive Remote Desktop
connections.

1. Head to the `Network -> Security Groups` item in menu on the left and
navigate to `Create Security Group`. Name the group `RDP` and click `Create
Security Group`
2. Select `Manage Rules` in the `Actions` column for your new security group
3. Select `+ Add Rule`
4. Under `Rule` select `RDP`
5. Under CIDR specify the IP range that will require access to your instance.  
**Do NOT leave as `0.0.0.0/0`.** Strongly consider limiting the IP address
range for your RDP security group to `10.0.0.0/8` which includes the ranges
used by the university VPN. This means you’ll have to open a VPN connection
before connecting to your instance, but will drastically reduce the potential
for unauthorised access.  
![](../img/windows_10.png)

6. Add the security group to your instance. Head to the `Compute -> Instances`
item in menu on the left and select the dropdown-menu for your instance and
choose `Edit security groups`. Select the + sign next to the `RDP` security
group to apply the security group rules to your instance.

7. Select the dropdown-menu for your instance again and choose `Console`.  
Login to your instance and open `System` by right-clicking the Windows start
icon in the bottom-left corner and select `System`  
![](../img/windows_11.png)

8. Select `Remote settings`
![](../img/windows_12.png)

9. Select `Allow remote connections to this computer` and `OK`
![](../img/windows_13.png)

You should now be able to connect to your instance using an RDP client.
