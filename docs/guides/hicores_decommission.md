# Decommission of the 'hicores' flavor

The Melbourne Research Cloud offers different types of virtual machines (flavors) for different purposes.

The main ones are uom.general (our general purpose flavors), uom.bigmem (our big memory flavors) and uom.hicores (our high cores flavors).

Each of these flavors uses specific hardware nodes.

The hicores nodes are now out of warranty/usable service life, and will be decommissioned on 30 April 2024.

The hicores nodes will not be replaced. They were originally purchased to provide a higher RAM to CPU ratio, but with the increase in performance and specifications of the general purpose nodes, the functionality of hicores has been taken on by the general purpose nodes.

The hicores nodes on the MRC will be decommissioned on 30 April 2024. We will remove the ability to launch VMs using the hicores flavors on 15 January 2024

This means that all existing hicores flavor virtual machines will need to be migrated to uom.general flavors so the underlying hardware nodes can be decommissioned. 

## How to check if you have a hicores flavor.

To check if you are affected by this change, navigate to the [MRC dashboard](https://dashboard.cloud.unimelb.edu.au) and click 'Compute' > 'Instances'. This will display a list of instances in your project. (Please note: if you are a member of more than one project, you will need to check for each project by toggling the 'PROJECTS' drop-down list at the top right of the screen.)

Once you have the list of instances in the project on screen, check in the 'Flavor' column. If you have an instance with 'hicores' in the name, then you are affected and will need to take action.

![](../img/hicores_flavor.png)

## What you need to do if you have a hicores flavor.

If you have a hicores flavor, you will need to resize it to a 'uom.general' flavor before 30th April 2024.

To perform a resize, click on the 'Actions' drop-down at the far right of the column with the affected virtual machine.

![](../img/hicores_resize.png)

Once the menu appears, you will be able to select a new flavor.

![](../img/hicores_resize_new_flavor.png)

* NOTE: You will need to select a new flavor with the 'uom.general' prefix.

During the resize process, your virtual machine will reboot and be inaccessible until the resize is complete.

If you no longer need your hicores flavor virtual machine, please consider deleting it to save resources and allow other researchers to use the MRC.

## Correspondence of legacy flavors to general flavors

Use the table below to assist you with the selection of a flavor for resizing; if the target general flavor is not listed on your project please log a ticket from the dashboard "Support" select "Submit a ticket".

<div align="center">
<table>
	<thead>
		<tr>
      <td><strong>Legacy flavor</strong></td>
      <td><strong>General flavor</strong></td>
		</tr>
	</thead>
    <tr>
        <td>uom.hicores.8c72g</td>
        <td>uom.general.8c32g</td>
    </tr>
    <tr>
        <td>uom.hicores.12c108g</td>
        <td>uom.general.12c48g</td>
    </tr>
    <tr>
        <td>uom.hicores.16c144g</td>
        <td>uom.general.16c64g</td>
    </tr>
    <tr>
        <td>
            uom.hicores.20c180g<br>
            uom.hicores.24c216g<br>
            uom.hicores.28c252g
        </td>
        <td>uom.general.24c96g</td>
    </tr>
   <tr>
        <td>uom.hicores.32c288g</td>
        <td>uom.general.32c128g </td>
    </tr>
    <tr>
        <td>uom.hicores.48c432g</td>
        <td>uom.general.48c192g</td>
    </tr>
    <tr>
        <td>
            uom.hicores.64c576g<br>
            uom.hicores.80c720g
        </td>
        <td>uom.general.64c256g</td>
    </tr>
</table>
</div>

## What if your instance uses a general flavor and is on a hicores host

Unfortunately for some instances that were created some time ago got created on a hicores host, these instances also need to be resized.

You can resize it to a smaller flavor, or if the quota allows to a bigger flavor, or you can resize it to a small flavor and then back to the original flavor,  the resize process will take them out from the hicores hosts.

When you resize your virtual machine, the virtual machine will reboot and be inaccessible until the resize is complete.

If you are not using your virtual machine, please consider deleting the virtual machine to free up resources.

If you have not resized your virtual machine by 30 April 2024, we will do it for you, as the hosts are now out of warranty/usable service life.

To resize your instance please follow the instructions below:

click on the 'Actions' drop-down at the far right of the column with the affected virtual machine.

![](../img/hicores_resize.png)

Once the menu appears, you will be able to select a new flavor.

![](../img/hicores_resize_general_flavor.png)

* NOTE: You will need to select a new flavor with the 'uom.general' prefix.

During the resize process, your virtual machine will reboot and be inaccessible until the resize is complete.

If you no longer need your hicores flavor virtual machine, please consider deleting it to save resources and allow other researchers to use the MRC.

## List of hicores hosts

<div align="center">
<table>
    <thead>
        <tr>
           <td><strong>hicores hosts</strong></td>
    </thead>
    <tr>
     <td>
         qh2-rcc207<br>
         qh2-rcc208<br>
         qh2-rcc209<br>
         qh2-rcc210<br>
         qh2-rcc211<br>
         qh2-rcc212<br>
         qh2-rcc213<br>
         qh2-rcc214<br>
         qh2-rcc215<br>
         qh2-rcc216<br>
         qh2-rcc217<br>
         qh2-rcc218<br>
         qh2-rcc219<br>
         qh2-rcc220<br>
         qh2-rcc221<br>
         qh2-rcc222<br>
         qh2-rcc224<br>
         qh2-rcc225<br>
         qh2-rcc226<br>
         qh2-rcc228<br>
         qh2-rcc229<br>
         qh2-rcc231<br>
         qh2-rcc232<br>
         qh2-rcc233<br>
         qh2-rcc234<br>
         qh2-rcc236<br>
         qh2-rcc238<br>
         qh2-rcc239<br>
         qh2-rcc240<br>
         qh2-rcc241<br>
         qh2-rcc243<br>
         qh2-rcc244<br>
         qh2-rcc245<br>
         qh2-rcc246<br>
         qh2-rcc247<br>
         qh2-rcc248<br>
         qh2-rcc249<br>
         qh2-rcc251<br>
         qh2-rcc252<br>
         qh2-rcc254<br>
         qh2-rcc255<br>
         qh2-rcc256<br>
         qh2-rcc257<br>
         qh2-rcc258<br>
         qh2-rcc260<br>
         qh2-rcc261<br>
         qh2-rcc262
    </td>  
   </tr>
</table>
</div>
