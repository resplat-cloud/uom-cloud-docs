<!--- This page is redirected to /security.md go to mkdocs.yml to update redirection --->
# Security Advice for the Melbourne Research Cloud
<sup>Version 0.1</sup>

Virtual machines (VMs) are accessed remotely, and therefore require different 
security considerations to your physical computer.  On one hand, physical 
barriers won't stop someone accessing your VM because they can connect to it 
over the Internet, just like you.  On the other hand, you can't accidentally 
drop your VM in the river.

Just like protecting your physical computer requires some sensible precautions, 
a VM can also be protected.  In fact in many ways, a VM is much easier to 
protect, if you know what you are doing.

This guide will provide some basic advice for securing your Melbourne Research 
Cloud (MRC) VM.  It is not an in-depth cloud security course, but it will help 
you keep your VM safe from the majority of threats you might encounter.

## Follow the guides
First and foremost, when you are creating a VM, follow the "Getting Started" 
guide on the MRC documentation page: https://docs.cloud.unimelb.edu.au/training/

This guide will take you through the creation process, including security 
considerations, one step at a time.

## SSH and SSH Keys
SSH is short for "Secure SHell" and is a protocol for communicating with your 
VM over the internet.  As the name implies, it is secure, in that all the data 
transferred over this protocol to and from your VM is encrypted.  This means 
if someone were to be watching your data transfers, the data would be 
completely useless to them.

Your data is encrypted using a special key, called an SSH key.  The key is 
made up of two components, a private key and a public key.  These keys need to 
work together to decrypt your data; neither one alone is enough.  The public 
key is connected to the remote end of the transfer, in this case, your VM.  
Your public key can be shared because any data encrypted with it cannot be 
decrypted without the private key.  The private key is something you should 
keep private.  It is not to be shared.  Anyone with your private key can 
decrypt any data encrypted with your public key.  And without it, not even you 
can decrypt your encrypted data.

So keep it safe, don't lose it.

As SSH is the front door to your Linux VMs, consider implementing some of the 
SSH hardening tips given by the Linux Handbook. In particular, we recommend 
disabling empty passwords (!) and also disabling password based SSH logins.

## Patching and updates
Even if you are using SSH and keeping your ssh keys safe, there are other ways 
your VM can be compromised.  Sometimes applications and operating systems have 
vulnerabilities that nefarious individuals might exploit.  There are many 
security experts checking and rechecking to find these vulnerabilities before 
the hackers can exploit them, and you will almost certainly have been prompted 
at some point to install a software patch or update for your computer or a 
particular application.  These patches and updates often contain security 
fixes for discovered vulnerabilities.  It is usually a very good idea to 
install them as soon as possible.

The only time it may be a good idea to wait before you install a patch is if 
there is a risk it could cause problems with your software.  If you think this 
could happen, you should make sure you have a snapshot of your VM before you 
proceed.

It is also critical that you confirm the source of the patch.  You should only 
install patches or updates downloaded from the company that created the 
operating system or application, e.g. Windows patches should only be 
downloaded from Microsoft.  At the University of Melbourne, you can also trust 
department and central service fileshares, as they files there have been 
downloaded from the authoritative source.

It is important to keep up-to-date with patches and updates. Note that you 
need to update both your operating system and the applications that you have 
installed on your VMs. Either set your operating system or application to 
automatically download and install updates, or make sure you check for 
updates regularly.

##Ports
You can connect to your VM through open ports, such as port 80 for web 
traffic (http), port 443 for secure web traffic (https) or port 22 for ssh.  
Ports are used for different types of traffic and represent a way in to your 
VM.  Some ports are more secure than others, for example, https is more secure 
than http.  Where possible, you should always try to use a secure protocol to 
access your VM, and only use insecure protocols when absolutely necessary.

Even using a secure port may not be enough.  Sometimes hackers find ways to 
break in through a secure port because of an undiscovered vulnerability.  
While this is uncommon, it does happen.  So to protect yourself, you should 
only open ports that you really need.  For example, if you aren't planning to 
run a web service on your VM, you don't need to have ports 80 or 443 open.

On the MRC, you will usually want port 22 for ssh to be available.  Combined 
with your private key, this provides a simple way to access and manage your 
VM securely, and presents a very difficult to break target for hackers.  Even 
so, you should consider if you really need to keep port 22 open all the time, 
especially if you have sensitive data on the VM.  You will usually need port 
22 open to set up and configure your VM, but if you are using your VM for 
long-running processing, you can use the MRC dashboard to disable port 22, 
rendering your VM completely inaccessible.  This won't stop the VM from 
continuing to process your data, but neither you nor any hackers will be able 
to get in, until you re-enable port 22.

##Private IPs
Using Private IPs are a great option if you don't need to access your virtual 
machine from the wider internet without using the VPN service.  Private IPs 
are safer than public IPs as any attack must originate from the internal 
network.  While they are not a guarantee of security, they are an important 
piece of the puzzle.  We recommend only using a public IP if it is absolutely 
necessary, and we only provision VMs with public IPs via an 
[approved request](https://uomservicehub.service-now.com/esc?id=sc_cat_item&sys_id=7459b2eec3947910847c339f05013152).
And when we do provision them, they come with [operational guidelines](https://gateway.research.unimelb.edu.au/__data/assets/pdf_file/0010/4672963/Operational-guidelines_Public-IP-usage-in-research-computing_2.pdf)
that must be followed by the user.

##Remote Desktop Protocol (RDP)
Windows RDP is a very popular target for hackers, so you should always avoid 
using it on a public network if at all possible.  If it is essential, you will 
need to actively maintain the security and health of your virtual machine, and 
be prepared for possible compromises.  We also regularly monitor our network 
traffic for RDP communications, and you will need to inform us if you intend 
to or are running a server this way.  A much better option is to use the 
private network and access your Windows VM via the VPN.

##Antivirus
Content available to UoM staff only [(link)](https://unimelb.service-now.com/kb_view.do?sysparm_article=KB0028265)

## Vulnerability Scanning agent
Content available to UoM staff only [(link)](https://unimelb.service-now.com/kb_view.do?sysparm_article=KB0028265)
##Security KBA and other resources

* [Linux Handbook](https://linuxhandbook.com/)
* [UoM Security KBA](https://unimelb.service-now.com/research?id=kb_article&sys_id=51b91d22dbc5c418f9748a264a961989)
