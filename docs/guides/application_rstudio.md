# Launching an RStudio Application
Not everyone can imagine learning the command line or Linux. We understand that
it is not an easy or approachable interface, and sometimes you just need to get
some work done.

But the work doesn't work on your laptop or it takes hours or the data wont fit
on your hard drive or in memory.

In these situations, we can spin up an instance of RStudio and access it 
through the web. Users still have full control of the machine if they 
want, but it's not necessary to ever manage the server in the same way
it is with our other offerings.

The disadvantage is that we can't provide much help beyond showing you how to
set these Software As A Service systems up. Further, these applications wont
be running as up to date software as if you managed it yourself.

Once you have a Project, follow these step to set up an RStudio (or Jupyter)
application.

## 1. Preparation

__N.B.__ Neither of these steps are strictly necessary to launch an 
Application. Despite this we list them here because our use has shown that 
having both will make your experience a lot smoother.

In preparation for the launching the application, you will need to have an `SSH
keypair` - [step two](/training/first_instance#create_keypair) on the getting 
started guide.

You will also need a decent size Volume. Follow [steps one and 
two](/guides/volumes/) to create a Volume.

The Application launch process requires the ID of the Volume. Click on the 
Volume's name and copy the id number. 

![](../img/create_volume_3.png)

Paste it into a text pad just in case.

![](../img/create_volume_4.png)

## 2. Create an Application

Head to the `App Catalog` item in menu on the left and navigate to 
`Browse -> Browse Local`

![](../img/app_step_1.png)

In the search bar marked with the work "Filter" search for rstudio. Once you 
have a result, choose `Quick Deploy`

![](../img/app_step_2.png)

Select one of the instance flavours available from the UoM Cloud, add a 
keypair, and select the melbourne-qh2-uom Availability Zone.

![](../img/app_step_3a.png)

On the next screen, add the Volume ID you collected in the Preparation step.
On the following screen, add a username and password combination, and then 
give the Application a name. Remember what username and password you used or
you will be locked out of the instance once it's created.

At this point you should see an RStudio application under the components tab on
the `App Catalog > Environment` screen of the dashboard. And hopefully it's
marked `Ready to deploy`.

![](../img/app_step_4.png)

Hit the `Deploy this environment` button. This will take a little time as the
system is built, but then you will have an IP address which you should be a 
link to the web interface offering you RStudio, Shiny and a desktop. Use the 
login name and password you set up above.

![](../img/app_step_5.png)
![](../img/app_step_6.png)









