# File transfer

## Command Line Interface

Using the CLI, the easiest way to transfer files is using the scp command (Secure CoPy).
scp is installed by default on all Linux and OSX machines and can be used in either
direction. Because it's based on ssh, you will recognise the `-i mykey.pem` syntax. 

### Secure copy
```
scp -i mykey.pem <from> <to>
``` 

While scp can be used in either direction by changing which computer we run the command 
from and which is in the `<from>` field and which is in the `<to>` field, in practice we 
don't always have a public IP address on our computers. So we always want to be executing
this command on our local computer so that we can use the cloud instance's public ip 
123.123.123.123

Copy files from my computer to the cloud instance
```
scp -i .ssh/mykey.pem filename.txt ubuntu@123.123.123.123:/home/ubuntu/
```

Copy files from the cloud instance to my computer. In this instance we will use a full 
stop character - `.` - to indicate "_the directory I am currently in_"
```
scp -i .ssh/mykey.pem ubuntu@123.123.123.123:/home/ubuntu/filename.txt .
```

But you could just as easily type the whole path
```
scp -i .ssh/mykey.pem ubuntu@123.123.123.123:/home/ubuntu/filename.txt /home/ubuntu/Desktop
```

On __OSX__ you might see a path more like `/Users/bernard/Desktop` instead of `/home/ubuntu/`

### To transfer multiple files 

To transfer more than one file at a time, there are a number of options:

* you can put multiple files in the `<from>` or `<to>` fields
* you can create a loop to scp multiple files
* you can use rsync to copy entire folders at a time

Backing up my Music folder to my cloud instance. This will create a directory called
Music in the home directory in the cloud:
```
rsync -av /home/bernard/Music -e ssh ubuntu@123.123.123.123:/home/ubuntu/
```

Copying my computation results from the cloud instance to my computer. Note that this
will create a folder called results in my home directory:
```
rsync -ave ssh ubuntu@123.123.123.123:/home/ubuntu/experiment/results /home/bernard/
```  

Rsync has one distinguishing feature - if the copy is interrupted, you can resume where 
you left off. It _synchronises_ files across the net.

There's a lot more to rsync and scp, but that should get you started.

## Windows

On Windows, we use and recommend [WinSCP](https://winscp.net/eng/downloads.php)
[FileZilla](https://filezilla-project.org/download.php?type=client) is another very good 
alternative but we will describe how to set up WinSCP. 

If you don't have administrative privileges, you can use the "Portable" app that listed 
on that page.

### Using WinSCP

When you start, you will see this screen. Fill out the details drop down the menu to 
choose SCP, put in the IP address and the user name, then click the Advanced drop down
menu.

  ![WinSCP_1](../img/winscp_1.png)


Within that menu, click the bold Advanced again.

  ![WinSCP_2](../img/winscp_2.png)


From there, choose Authentication under SSH, and choose your file.

  ![WinSCP_4](../img/winscp_4.png)


You will need to make the files visible by changing the visible file types. When you 
select your mykey.pem file, you will see a pop up message telling you that WinSCP will
need to convert your file to a `ppk` file. You can allow this to happen - save it in the
same folder as the `mykey.pem`. WinSCP will use the `mykey.ppk`. 
  
  ![WinSCP_5](../img/winscp_5.png)
  

If you then click login, you will be shown a warning that you can click yes on.

  ![WinSCP_7](../img/winscp_7.png)
  
You will now see a traditional file browser with your local files on the left and your 
cloud instance on the right. You can drag and drop files as you need.

  ![WinSCP_9](../img/winscp_9.png)

## OSX

On Apple's OSX you can use the command line above - but if you would like a GUI we use 
and recommend [CyberDuck](https://cyberduck.io/download/). You can use it for free but 
there will be ads poking you occasionally. 
[FileZilla](https://filezilla-project.org/download.php?type=client) is another very good 
alternative but we will describe how to set up CyberDuck.

### Using CyberDuck
When you open the softer you will immediately get the opportunity to fill out the 
necessary details. You will need to drop down the top menu to choose SFTP. 

  ![CyberDuck_1](../img/cyberduck_1.png)


Once you have successfully connected, you can save this as a Quick Connect link, so that
you don't need to fill the details in every time.

  ![CyberDuck_2](../img/cyberduck_2.png)

The Quick Connect is a shortcut to thatgroup of settings.

  ![CyberDuck_3](../img/cyberduck_3.png)

Then you just drag and drop between Finder and CyberDuck.

  ![CyberDuck_4](../img/cyberduck_4.png)

You will see a screen like this once successfully completed.

  ![CyberDuck_5](../img/cyberduck_5.png)
