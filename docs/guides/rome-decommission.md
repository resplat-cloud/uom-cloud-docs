# Decommission of the AMD EPYC 7702 "Rome" based compute nodes

### Date: January 2025
### Decommission completion due date: 1 March 2025

> **NOTE:** Please only follow these instructions if you have been directed to by RCS staff.

Our AMD-based compute hosts in the 'melbourne-qh2-uom_production' aggregate are out of warranty and are being decommissioned. These hosts run on the AMD EPYC 7702 "Rome" CPU, which is now superseded by the AMD EPYC 7763 "Milan" and AMD EPYC 9474F "Genoa" chipsets we currently offer in the MRC.
 
The decommission process requires all virtual machines (VMs) running on the Rome hosts to be resized to a different hardware flavor. 

In order to complete this process, we require some assistance from you. 
 
Your Rome VMs will need to be resized to a new flavor on the AMD EPYC 9474F "Genoa" aggregate. You can identify these flavors by the suffix 'v3' at the end of the flavor name. Older, Rome-based flavors do not have this suffix.
 
For example, if you have a VM that is currently on the 'uom.general.4c16g' flavor, you will need to resize it to the 'uom.general.4c16g.v3' flavor. The resize process will move your VM from the AMD Rome to the newer AMD Genoa compute hosts.

> **NOTE:** Very old versions of Linux will not work on the Genoa CPUs. For example, Ubuntu 16.04 will not launch on Genoa, but it will launch on the Milan aggregate. If you are in doubt, and you require a very old version of Linux (ie greater than 7 years old), please open a support ticket to discuss your options with RCS [here](https://uomservicehub.service-now.com/esc?id=sc_cat_item&sys_id=5eef1dcbdbe9a74035dc403c3a9619f5).

The resize process requires a system reboot, so it may be necessary to schedule the resize during other system maintenance periods.

The following table shows the old Rome-based flavor and its corresponding Genoa-based flavor. Please use it to determine which flavor you should resize to.

| AMD Rome flavor     | AMD Genoa flavor       |
|:-------------------:|:----------------------:|
| uom.general.32c128g | uom.general.32c128g.v3 |
| uom.general.8c32g   | uom.general.8c32g.v3   |
| uom.general.24c96g  | uom.general.24c96g.v3  |
| uom.general.20c80g  | uom.general.20c80g.v3  |
| uom.general.12c48g  | uom.general.12c48g.v3  |
| uom.general.48c192g | uom.general.48c192g.v3 |
| uom.general.2c8g    | uom.general.2c8g.v3    |
| uom.general.16c64g  | uom.general.16c64g.v3  |
| uom.general.28c112g | uom.general.28c112g.v3 |
| uom.general.4c16g   | uom.general.4c16g.v3   |
| uom.general.1c4g    | uom.general.1c4g.v3    |
| uom.general.64c256g | uom.general.64c256g.v3 |

## Resize process: MRC dashboard

To resize your instance please follow the instructions below:

Click on the 'Actions' drop-down at the far right of the column with the affected virtual machine.

![](../img/rome-decomm-resize-drop-down.png)

Scroll down and select 'Resize Instance'.

Once the menu appears, you will be able to select a new flavor.

![](../img/rome-decomm-resize-modal.png)

To launch on the new hardware, select the same flavor name with the `v3` suffix.

During the resize process, your virtual machine will reboot and be inaccessible until the resize is complete.

## Resize process: Openstack CLI

To resize using the Openstack CLI, use the following command structure:

```
openstack server resize --flavor <target-flavor> <server-ID>
```

## Resize process help

If you are unable to perform the resize or have any questions, please open a support ticket with RCS [here](https://uomservicehub.service-now.com/esc?id=sc_cat_item&sys_id=5eef1dcbdbe9a74035dc403c3a9619f5).

## Resize process: Research Computing Portal (RCP) users

### Research Server

If you have been directed to resize a Research Server on the RCP, please use the following instructions.

Research Servers using the outdated `(Gen 1)` flavor/size are required to resize to a new flavor. These new flavors do not have the `(Gen 1)` prefix. For example, if you are currently on `(Gen 1) 4 vCPU 16GB` you will need to resize to `4 vCPU 16GB`.

Please note this does not apply for Research Servers currently using any of the `Big Memory` or `GPU` flavors and sizes.

IMPORTANT: Following the instructions below will reboot your Research Server. Make sure you have completed any important processes before starting.

#### Select 'manage'

![](../img/rome_decomm_manage_RCP.png)

#### Select 'resize'

![](../img/rome_decomm_resize_RCP.png)

#### Select the target flavor

Select the same flavor value as the existing resource. For example, if your Research Server is currently `(Gen 1) 4vCPU 16GB`, select the `4 vCPU 16GB` size.

![](../img/rome_decomm_choose_resize_RCP.png)

#### Confirm 'resize'

IMPORTANT: This step will reboot your Research Server. 
![](../img/rome_decomm_confirm_resize_RCP.png)

This process takes about 10 minutes and will move your Research Server from the deprecated hardware to the new hardware.

### RStudio and Shiny Server

If you have been directed to resize a RStudio and Shiny Server on the RCP, please use the following instructions.

IMPORTANT: Following the instructions below will reboot your RStudio Server. Make sure you have completed any important processes before starting.

#### Select 'manage' 

![](../img/rome_decomm_manage_desktop_RCP.png)

#### Select 'boost'

![](../img/rome_decomm_boost_desktop_RCP.png)

#### Confirm 'boost'

IMPORTANT: This step will reboot your RStudio Server. 
![](../img/rome_decomm_boost_confirm_desktop_RCP.png)

Wait for the boost process to complete (about 10 minutes).

#### Select 'downsize' 

![](../img/rome_decomm_downsize_desktop_RCP.png)

#### Confirm 'downsize'

IMPORTANT: This step will reboot your RStudio Server. 
![](../img/rome_decomm_downsize_confirm_desktop_RCP.png)


Wait for the downsize process to complete (about 10 minutes).

Once the downsize process is complete, your RStudio Server will have moved from the deprecated hardware to the new hardware.

## Resize process help

If you are unable to perform the resize or have any questions, please open a support ticket with RCS [here](https://uomservicehub.service-now.com/esc?id=sc_cat_item&sys_id=5eef1dcbdbe9a74035dc403c3a9619f5).

### Researcher Desktop 

If you have been directed to resize a Researcher Desktop on the RCP, please use the following instructions.

IMPORTANT: Following the instructions below will reboot your Researcher Desktop. Make sure you have completed any important processes before starting.

#### Select 'manage' 

![](../img/rome_decomm_manage_desktop_RCP.png)

#### Select 'boost'

![](../img/rome_decomm_boost_desktop_RCP.png)

#### Confirm 'boost'

IMPORTANT: This step will reboot your Researcher Desktop. 
![](../img/rome_decomm_boost_confirm_desktop_RCP.png)

Wait for the boost process to complete (about 10 minutes).

#### Select 'downsize' 

![](../img/rome_decomm_downsize_desktop_RCP.png)

#### Confirm 'downsize'

IMPORTANT: This step will reboot your Researcher Desktop. 
![](../img/rome_decomm_downsize_confirm_desktop_RCP.png)


Wait for the downsize process to complete (about 10 minutes).

Once the downsize process is complete, your Researcher Desktop will have moved from the deprecated hardware to the new hardware.

## Resize process help

If you are unable to perform the resize or have any questions, please open a support ticket with RCS [here](https://uomservicehub.service-now.com/esc?id=sc_cat_item&sys_id=5eef1dcbdbe9a74035dc403c3a9619f5).
