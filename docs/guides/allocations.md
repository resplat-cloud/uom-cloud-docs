# Managing Allocation Requests

This guide includes [instructions](#allocation-request-lifecycle) for getting a
new allocation of cloud resources ([project](#what-is-an-allocation-request))
on the Melbourne Research Cloud, and how to manage those resources after that.

## What is an Allocation Request?

The main organisational structure on the Melbourne Research Cloud is the
Project. When you create an instance, volume or object store container they
belong to a specific Project. Once you have logged into 
[the dashboard](https://dashboard.cloud.unimelb.edu.au), you can change Project
in the `Projects` dropdown in the top-right navigation bar. The selected 
Project will be what dictates the resource limits and in which subsequent 
actions are made.

You may submit an `Allocation Request` to apply for a new Project and request 
an allocation (quota) of cloud resources. The Allocation Request may also be
amended at any time to request more resources, update any details or
extend the Project. Resources are allocated for a limited length of time to
ensure that the Melbourne Research Cloud is being actively used.

Any user may have, or belong to, any number of Projects.

## Where do I create or manage my Allocation Requests?
Allocation Requests are managed in the 
[Melbourne Research Cloud dashboard](https://dashboard.cloud.unimelb.edu.au) 
using the "Allocations" tab on the left-hand navigation panel:

* A new allocation request can be submitted at [Allocations > New Request](
  https://dashboard.cloud.unimelb.edu.au/allocation/).
* Your list of allocation requests can be found under [Allocations > My
  Requests](https://dashboard.cloud.unimelb.edu.au/allocation/user_requests/).

**Note:** The only user who can see projects under My Requests is the user who
created the allocation request. In other words, if a Project needs to be 
extended or updated in any way, that request needs to come from the Project
Manager(s) also known as Tenant Manager(s).

![](../img/allocation-myrequests.png)

## Allocation Request Lifecycle

### Submission
You can submit a new allocation request at [Allocations > New Request](
https://dashboard.cloud.unimelb.edu.au/allocation/).

See the below section [Requestable Cloud Resources](#requestable-cloud-resource
s) for help in completing the Cloud Resources section.

Allocation requests can take up to 2 weeks to process.

Your allocation request may be declined. If this occurs you will receive an
email with advice regarding the reasons why. You may be asked to amend and
resubmit your original allocation request.

### Approval
If your allocation request is approved you will receive an email telling you
that your project has been provisioned.

The start date will be updated to the date of approval if the approval occurs
later than your specified start date.

### Renewal
First Check that you have selected the appropiate project

![](../img/allocation-projectselection.png)

On the allocations menu select "My Requests" to see your project, under the "Actions" column select "Amend/Extend allocation"

![](../img/allocation-select-extension.png)

In the form fill in the extension duration

![](../img/allocation-extension-duration.png)

and at the bottom of the form read, agree to the Terms and Conditions, and submit your application.

![](../img/allocation-agree-tc.png)


### Expiry
Your project will have an end date. You will be reminded that your project will
be expiring two weeks before the end date. You may extend the allocation
request at any time in the dashboard.

If you do not submit an extension request:

1. On the expiry date the allocation quotas will be set to zero. Existing
   virtual machines and storage resources will continue to be accessible, but
   no new resources can be created.
2. Two weeks after expiry, all instances and data in this project allocation will
   be archived and safely stored.
3. Three months after expiry, all archived instances and data will be deleted.

# Requestable Cloud Resources

## Compute Resources

The following flavours can be launched on the Melbourne Research Cloud. Use
this table to determine the number of Instances and vCPUs to include in your
Allocation Request form. All instances come with a 30 GB disk.

<table>
<tr>
<th>Flavour name</th>
<th>vCPUs</th>
<th>RAM (GB)</th>
<th>Suggested use</th>
</tr>
<tr>
<td>uom.general.1c4g</td>
<td>1</td>
<td>4</td>
<td>Simple web hosting</td>
</tr>
<tr>
<td>uom.general.2c8g</td>
<td>2</td>
<td>8</td>
<td>Database driven website</td>
</tr>
<tr>
<td>uom.general.4c16g</td>
<td>4</td>
<td>16</td>
<td>Data Science using RStudio or JupyterHub</td>
</tr>
<tr>
<td>uom.general.8c32g</td>
<td>8</td>
<td>32</td>
<td>Data science on larger data sets</td>
</tr>
<tr>
<td>uom.general.12c48g</td>
<td>12</td>
<td>48</td>
<td></td>
</tr>
<tr>
<td>uom.general.16c64g</td>
<td>16</td>
<td>64</td>
<td></td>
<tr>
<td>uom.general.20c80g</td>
<td>20</td>
<td>80</td>
<td></td>
</tr>
<tr>
<td>uom.general.24c96g</td>
<td>24</td>
<td>96</td>
<td></td>
</tr>
<tr>
<td>uom.general.28c112g</td>
<td>28</td>
<td>112</td>
<td></td>
</tr>
<tr>
<td>uom.general.48c192g</td>
<td>32</td>
<td>128</td>
<td></td>
</tr>
<tr>
<td>uom.general.64c256g</td>
<td>64</td>
<td>256</td>
<td></td>
</tr>
</table>

Note: the General flavors run on oversubscribed hypervisors.

## Volume Storage

A persistent volume looks and acts like a hard drive that can be attached to
your virtual machine instances. Volumes and their data persist independently of
virtual machine instances and volumes can be dynamically attached and detached
to/from different virtual machine instances.

## Object Storage

Object Storage is a large accessible online storage location that you can reach
from most machines with internet connectivity. Object Storage requires cloud
native applications for access.

## Database Service

The database service provides a simple interface for provisioning and managing
database engines supporting MySQL and PostgreSQL, with MongoDB coming soon. You
can specify the number of database servers, the total database storage your
project would require, or both.

## Advanced Networking

Virtual resources for building more advanced network topologies for your
instances, including routers, load balancers and private networks.
