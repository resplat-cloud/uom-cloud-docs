# FAQ

For miscellaneous jargon and concepts, such as what cloud computing is, please refer to the [Concepts](training/concepts.md) page in our Getting Started documentation.

## Why would I use the cloud?

Two psychology researchers at an Australian university, needed to do some fairly complex statistical analysis.
So out of their grant money they bought two powerful computers, at a cost of $10 000 per machine to run MatLab on.
But on the first run, they found that the machines couldn’t work with their data sets.
The machines were underpowered!
They were forced to iteratively upgrade those machines, until finally, one was able to perform the required analysis.
It had taken them 12 months from the date of purchase to get to this point.

If, instead of purchasing hardware, they had turned to the cloud, they would have been able to have had their initial machine running within minutes.
If it was underpowered, they would have been able to upgrade it to a larger machine, again within a matter of minutes.
They probably wouldn’t even have had to re-install the software ...

## I'm sold. How much does it cost a researcher to use the Melbourne Research Cloud?

It’s currently free at the point of service for University of Melbourne researchers.

## I'm not at the University of Melbourne. What about me?

If you are a researcher, there is a national federated cloud hosted by [ARDC] that you may be able to use.

Otherwise, there are many commercial cloud vendors who will be happy to help you.

## Is the Melbourne Research Cloud a public cloud?

No: it is a private cloud at the University of Melbourne.

## How do I start using the Melbourne Research cloud?

Usage of the Melbourne Research Cloud requires approval of at least one project.
To request a project, go to the [New Request Form](https://dashboard.cloud.unimelb.edu.au/allocation/).

More about how to request and manage an allocation is in our [Allocations Guide](guides/allocations.md).

## Can I personalise my cloud servers?

You will have administrative access to each cloud server you launch.
Thus all system administration, including software installation and configuration, is your responsibility.

## Are my cloud servers backed up up for me?

No. Backing up your servers and your data is your responsibility.
Taking an image (i.e. a snapshot) of your servers is relatively easy through the dashboard, but this is not automated in any way.

Also, we do recommend that you keep backups of your data off of the cloud, just in case of catastrophe.

## What is the difference between volume backups and volume snapshots?

Openstack has a "volume backup" and a "volume snapshot" function. Snapshots store a point in time snapshot of a volume on the same
storage cluster as the original volume. You can roll back to a snapshot if you need, e.g. if you delete a file. Note that if the cloud storage
system fails (however unlikely) your volume and snapshot would not be accessible.  

Volume backups store a copy of the volume on Swift, the cloud object store. Note that the maximum volume size that can be backed up is around 1TB,
due to limitations of the volume servers.  

## Can I use the object store for backups?

If you do use object storage for backups, do not exceed your allocated quota.
Also, files larger than 5G in size need special handling when uploading to, and downloading from, the object store.

The object store keeps at least three copies of each object in it.
While this does provide fault tolerance, there is no other backup process for these objects.
Therefore, the recommendation again applies that you should keep offline backups;
while we aim to provide a robust service, putting all your eggs in one basket is never a good idea.

## Can I share a volume with several servers at the same time?

No. You may wish to use NFS or CIFS shares if you need to make storage available to multiple devices.

## Can I resize my server while it is running?

No, you will have to restart the server.
Strictly, you can resize your server while it is on — but it will restart during the resize process.

## Should I use the AWS EC2 compatibility APIs?

There may be times when you have to use these, due to requirements of the software that you are using.
However, if you do use them, be aware that:

* This is not an Amazon cloud, so compatibility is incomplete.
* Problems encountered can be difficult to resolve.
* We do not officially support them; if you run into problems, we do not guarantee the ability to provide a lot of help.

So avoid them, and if you do have to use them, consider finding resources to migrate your AWS dependent code to the native OpenStack APIs.

## Can I run Windows on my cloud servers?

Yes, see this guide to [launching a Windows instance](guides/windows.md) on the Melbourne Research Cloud.

## I want to run MatLab on my cloud servers...

Here are instructions that we have tested for getting MatLab up and running
on an Ubuntu 18.04 instance with a Public IP:

1. Launch your Ubuntu VM
1. SSH into your VM: `ssh -Y -i mykeyname.pem ubuntu@ip.add.ress`
1. Update the software on it: `sudo apt-get update & sudo apt-get upgrade`
1. Install needed applications: `sudo apt-get install firefox unzip default-jre`
1. Launch firefox (firefox over ssh tunnel is very slow...)
1. Go to www.mathworks.com and log in (or create an account using the 
   University option)
1. Follow the instructions to download Linux installer
1. unzip the downloaded file
1. `sudo ./install` (you must use sudo so the installer can create directories)
1. Follow the installation instructions (keep in mind, the root disk is only 
   about 30GB, so don't try to install all of the available packages).
 
If these instructions aren't suitable for you, MatLab publishes 
[reference architectures](https://github.com/mathworks-ref-arch) for deploying 
on various cloud providers.

These show the patterns you should follow when configuring it on your cloud 
servers.

If you have questions, please 
<a class="button-small with-arrow soft" href="https://unimelb.service-now.com/research" target="_blank">request help</a>

## Does the Melbourne Research Cloud offer software as a service?

There are a small handful of applications that can be run through the App Catalog menu item — RStudio and Jupyter are the two primary offerings.
We only offer limited support for these products.

Otherwise, you are free to launch, install and manage your own instances and software on the Melbourne Research Cloud.

## How can I get help from a human?

<a class="button-small with-arrow soft" href="https://unimelb.service-now.com/research" target="_blank">Request Help</a>

[ARDC]: https://ardc.edu.au/services/ardc-nectar-research-cloud/
