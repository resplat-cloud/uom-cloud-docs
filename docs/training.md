# Getting Started

The following is a hands-on introduction to the Research Cloud.
It runs through some basic concepts, then provides a tutorial where we will create a computer in the cloud, connect to it, run some commands, and close it down again.

_Prerequisites:_ This training focuses on Linux.
While we provide all the commands you need to enter at the command line,
a basic familiarity with the command line will help you be most productive.
There are many online Linux resources, but the first few sections of
[this tutorial](http://www.ee.surrey.ac.uk/Teaching/Unix/) are a good place to start.
You'll also need a working SSH client on your computer.
If you are not familiar with SSH, we've provided
[this guide](guides/ssh_client.md) that explains how to set it up.


1. [What is the research cloud?](training/intro.md)
2. [Concepts](training/concepts.md)
3. [Your First Instance](training/first_instance.md)
