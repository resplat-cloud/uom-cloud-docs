# Guides

This section provides in-depth guides on particular topics related to the
Melbourne Research Cloud.

## News and Updates
* [Decommissioning of AMD EPYC 7702 "Rome" compute hosts 2025](guides/rome-decommission.md)
* [New AMD nodes deployed in 2024](guides/compute-refresh-2024.md)
* [Decommissioning of hicores flavors 2024](guides/hicores_decommission.md)

## Research Cloud Basics
* [Managing Allocation Requests](guides/allocations.md)
* [Getting access for an existing Nectar Project](guides/access_from_nectar.md)
* [Migrating from Nectar to MRC](guides/migrating_to_mrc.md)
* [Running basic computations](guides/compute.md)
* [Using the UoM Internal Network](guides/using_uom_internal_network.md)
* [Security Advice](guides/security.md)

## System Administration Basics
* [Setting up an SSH Client](guides/ssh_client.md)
* [Running a 24/7 server](guides/server.md)
* [Create, format and mount a Volume](guides/volumes.md)
* [Transferring files to and from a server](guides/file_transfer.md)
* [Launching an RStudio Application](guides/application_rstudio.md)
* [Launching a Windows Instance](guides/windows.md)

## Advanced
* [Advanced Networking Guide](guides/advanced_networking.md)
* [Applying for General Purpose GPUs](guides/gpu_access.md)
* [External Support and Training](guides/nectar_cloud_training.md)
