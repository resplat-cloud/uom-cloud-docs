# Melbourne Research Cloud End-User Documentation


This repository is for Melbourne Research Cloud end-user documentation, which includes entry-level user training and guides.

## Installation

MkDocs needs to be installed to use this repository.
You may either follow the
[MkDocs installation instructions](https://www.mkdocs.org/#installation)
or run the following command:

```sh
pip install -r requirements.txt
```

MkDocs has a built-in dev server which allows you to preview the documentation as you work on it.
The server can be started by running the following command from the base directory of this repository:

```
mkdocs serve
```

Once you are ready to build the site you may do so by running the following command:

```
mkdocs build
```

This will generate all of the required static files and place them in a `site/` directory.

If you are editing the documentation in Linux, the following command will automatically rebuild `site/` and reload the server:

```
inotifywait -qmre create --format "%f" ./ | grep --line-buffered "\.md$" | while read f; do mkdocs build; done
```
